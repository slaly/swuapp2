var gulp = require('gulp');

var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-ruby-sass');

var libJS = [
    'assets/jquery/dist/jquery.min.js',
    'assets/bootstrap/dist/js/bootstrap.min.js',
];

var libCSS = [
    'assets/bootstrap/dist/css/bootstrap.min.css', 
];


//compile scss to css แล้ว ย่อ ไฟล์
gulp.task ('css',function(){
	sass('assets/mylib/css/style.scss',{
    style: 'compressed'
	})
    .on('error',sass.logError)           //Compile and minify
	.pipe(rename({suffix: '.min'})) //หลัง minify เพิ่ม .min ต่อท้าย
	.pipe(gulp.dest('wwwroot/css')); 	   //dest : คือ Folder ที่ต้องการ Save (ถ้าไม่มีจะ Create)
});

//ย่อไฟล์
gulp.task ('js',function(){
    return gulp
    .src('./assets/mylib/js/script.js')    
    .pipe(minifyJS())
    .pipe(rename({suffix : '.min'})) //หลังminify เพิ่ม .min ต่อท้าย
    .pipe(gulp.dest('wwwroot/js'));
});

gulp.task ('libjs',function(){
    return gulp
    .src(libJS)    
    .pipe(gulp.dest('./wwwroot/js'));
});

gulp.task ('libcss',function(){
    return gulp
    .src(libCSS)    
    .pipe(gulp.dest('./wwwroot/css'));
});


gulp.task ('all',['css','js','libjs','libcss']);
